import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('button-lit')
export default class ButtonLit extends LitElement {

  @property({type : String}) label = 'Default Label';

  @property({ type: Number })
  count = 0

  render() {
    return html`
      <div class="button">
        ${this.label}
      </div>
    `
  }

  static styles = css`
    .button {
      background-color: yellow;
      padding: 4px 16px;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  `;
}

declare global {
  interface HTMLElementTagNameMap {
    'button-lit': ButtonLit
  }
}