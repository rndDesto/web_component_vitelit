import { LitElement, css, html } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('heading-lit')
export default class HeadingLit extends LitElement {

  render() {
    return html`
      <h1>
        <slot></slot>
      </h1>
    `;
  }

  static styles = css`
    h1 {
      background-color: red;
      padding: 4px 16px;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  `;
}

declare global {
  interface HTMLElementTagNameMap {
    'heading-lit': HeadingLit;
  }
}
